//
//  CoinManager.swift
//  ByteCoin
//
//  Created by Angela Yu on 11/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import Foundation

protocol CoinManagerDelegate {
    func didUpdatePrice(currency: String, price: Double)
    func didFailWithError(error: Error)
}

struct CoinManager {
    
    let baseURL = "https://rest.coinapi.io/v1/exchangerate/BTC"
    let apiKey = "859D02F2-51E7-4903-AC19-A75410BB9CBC"
    
    let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD","IDR","ILS","INR","JPY","MXN","NOK","NZD","PLN","RON","RUB","SEK","SGD","USD","ZAR"]
    
    var delegate: CoinManagerDelegate?
    
    func getCoinPrice(currency: String)
    {
        let urlString = "\(baseURL)/\(currency)?apikey=\(apiKey)"
        
        // make sure that the URL is valid
        if let url = URL(string: urlString) {
            
            // Get the session
            let session = URLSession(configuration: .default)
            
            // Give the Session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                
                if let safeData = data {
                    if let coinPrice = self.parseJSON(safeData) {
                        self.delegate?.didUpdatePrice(currency: currency, price: coinPrice)
                    }
                }
            }
            // Start the Task
            task.resume()
            
        }
    }
    
    func parseJSON(_ data: Data) -> Double? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(CoinData.self, from: data)
            let rate = decodedData.rate
            print(rate)
            return rate
        }
        catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
}
