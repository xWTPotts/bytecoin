//
//  CoinData.swift
//  ByteCoin
//
//  Created by Taylor Potts on 2020-08-03.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct CoinData: Codable {
    var rate: Double
}

